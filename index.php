<?php
// Load the database configuration file
include_once 'config.php';

// Get status message
if(!empty($_GET['status'])){
    switch($_GET['status']){
        case 'succ':
            $statusType = 'alert-success';
            $statusMsg = 'data has been imported successfully.';
            break;
        case 'err':
            $statusType = 'alert-danger';
            $statusMsg = 'Some problem occurred, please try again.';
            break;
        case 'invalid_file':
            $statusType = 'alert-danger';
            $statusMsg = 'Please upload a valid CSV file.';
            break;
        default:
            $statusType = '';
            $statusMsg = '';
    }
}
?>
 <html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
</head>
<!-- Display status message -->
<?php if(!empty($statusMsg)){ ?>
<div class="col-md-6">
    <div class="alert <?php echo $statusType; ?>"><?php echo $statusMsg; ?></div>
</div>
<?php } ?>
<body>
<div class="container">
<div class="row">
<h1 style="text-align:center;background-color:green;color:white">Import and Export CSV Data File</h1>
    <!-- Import & Export link -->
    <div class="col-md-6 head" style="margin:40px">
        <div class="float-right ">
            <a href="javascript:void(0);" class="btn btn-success" onclick="formToggle('importFrm');">Import</a>
            <a href="exportData.php" class="btn btn-primary"><i class="exp"></i> Export</a>
        </div>
    </div><br><br>
    <!-- CSV file upload form -->
    <div class="col-lg-6" id="importFrm" style="display: none;">
        <form action="importdata.php" method="post" enctype="multipart/form-data">
            <input type="file" name="file" /><br>
            <input type="submit" class="btn btn-primary" name="importSubmit" value="Upload">
        </form>
    </div>

    <!-- Data list table --> 
    <table class="table table-striped table-bordered">
        <thead class="thead-dark">
            <tr>
                <th>#ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // Get member rows
        $result = $conn->query("SELECT * FROM csvphp ORDER BY id ASC");
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
        ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['first_name']; ?></td>
                <td><?php echo $row['last_name']; ?></td>
                <td><?php echo $row['email']; ?></td>
            </tr>
        <?php } }else{ ?>
            <tr><td colspan="5">No member(s) found...</td></tr>
        <?php } ?>
        </tbody>
    </table>
</div>
</div>
<body>
<html>

<!-- Show/hide CSV upload form -->
<script>
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}
</script>