<?php
$servername="localhost"; 
$username="root"; // default user name for localhost is root. 
$password="";  // default password for localhost is empty.
$dbname="csvcorephp"; // database name

// create connection.
$conn = new mysqli($servername,$username,$password,$dbname);
//check connection
if ($conn->connect_error) 
{
   die ("Connection error" . $conn->connect_error);
}
?>